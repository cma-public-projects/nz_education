
## Education list creation
There is python code that takes the list of schools and student information, uses that to create student triples (age/sex/ethnicity) and matches that the inhabitants from the Dwellings Layer. Here we are needing to transform the reference data (directories) into a tidier format, and fixing any data quality issues as best we can.

### Schools
Last updated April 2020

The file `School_spatialdata_tidying.R`  cleans the school directory in several steps:

- Removes any school with 0 students
- Removes any school with out latitude/longitude (most were size 0, max was 10 students)
- Classify co-ed codes into simple groups (Girls, Boys, Co-ed)
- Convert longitude/latitude coordinates into SA2 codes.

Takes inputs: 

- `schooldirectory.csv`   from Dropbox/data/education/primary_highschool
- `SA2_2018_Land.*` set  from Dropbox/data/spatialdata/Shapefiles
- `SA2_TA18_DHB_unique.csv`   from Dropbox/data/spatialdata/concordances

Creates output: 

- `schoolDirectoryWithSpatial.csv` which is saved in Dropbox/data/spatialdata


### ECE
Last updated 2020/06/16

#### Step 1: file `ECE_spatialdata_tidying.R`  
This file cleans ECE data in several steps:

- Removes any school with 0 students
- Removes any school with out latitude/longitude (most were size 0, max was 10 students)
- Manually fixes coordinates for "Belfast Playcentre"
- Convert longitude/latitude coordinates/Census Area Units into SA2 codes.

Takes inputs: 

- `Directory-ECE-Current.csv`   from Dropbox/data/education/ECE
- `SA2_2018_Land.*` set  from Dropbox/data/spatialdata/Shapefiles
- `SA2_TA18_DHB_uniqueConcordance.csv`   from Dropbox/data/spatialdata/concordances

Creates output: 

- `ECEDirectoryWithSpatial.csv` which is saved in Dropbox/data/spatialdata

#### Step 2: file `ECE_upsampling.R` 

This file addresses internal inconsistencies we observe within the ECE data. We find that the total of demographic sub-groups (ethnicity, age) often to not sum to the same number as the `Total Roll`. This up-sampling processing step ensures that these counts will match, whilst maintaining the original proportions across groups. 

Takes input: 

- `ECEDirectoryWithSpatial.csv`   from Dropbox/data/spatialdata   *which was created in the previous step*

Creates output: 

- `ECEDirectoryWithSpatial.csv` which is saved in Dropbox/data/spatialdata



### Step 3: `get_nodelist_education`

Takes `schoolDirectoryWithSpatial.csv` and combines with `ECEDirectoryUpsampled.csv` , cleans using Dion's original python code (`complexContagion\Code\Python\networkBuilding\buildSchoolLayer.py`) and saves node list of educational institutions in outputs.