This folder stores the input files to create the school node list. These files include:
- `schooldirectory.csv`. This is obtained directly from education counts website: https://www.educationcounts.govt.nz/data-services/directories
Data obtained from educationcounts 26/03/20

- `Directory-ECE-Dec2019.csv`. Obtained directly from education counts website: https://www.educationcounts.govt.nz/directories/api-new-zealand-institutions

 
This folder also contains any additional files that are required for processing or analysis (e.g., concordances, shapefiles..)
