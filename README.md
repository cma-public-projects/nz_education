# NZ_Education

This repository holds data on educational institutions obtained from Education Counts.

## Instructions:
- Ensure that the repo is up to date on your machine. The notebook will save all files using the short ID of the git commit that is checked out in file/folder names, if you make changes locally and then run the notebook, it will still use the latest git commit ID. To make sure that file names are consistent with content, make sure to push any changes before running the notebook.
- Open `nz_education.Rproj`. This will open in RStudio. The `.Rproj` file ensures that all specified paths are relative to the main repo directory.
- Within RStudio, open `processing/construct_education_nodelist.Rmd`. This notebook will call all other code files and should run from start to finish to produce the node list.
- Ensure that all R packages are installed. these include: `here`,`tidyverse`,`splitstackshape`,`git2r`, `sf` .
- Set your local path to the dropbox folder `covid-19-sharedFiles/`. Ensure that the final backslash is included. After setting this string, the input files should all have the correct file names. 
- Check the file names (lines 49-58). Ensure that the files have been made available offline on dropbox. If they are stored as "online-only" it will cause an error later on. 
- Set the year of the data that you want. As of 2022-12-12 the dropbox contains data from 2022 and 2019. 
- After all of these steps, you should be able to `Run All` chunks (this can be done by navigating to the top right where it says `Run`, and clicking `Run All` on the drop down menu. The code will take roughly 30 minutes to finish, and it should produce a folder with the node list and sense checking files in an `outputs` folder in the repo, as well as a copy that goes to the `workplaces/nodelists/` folder in dropbox. 
- Double check the node list and the associated sense checking files to make sure the file looks reasonable before using. 
- Additional comments and descriptions of functions are described in `construct_education_nodelist.Rmd`.

Information on specific folders are detailed as follows:

## Folders

### processing

The `processing` folder contains the files used to process input data into a clean node list. To produce the education node list output, the main file `construct_education_nodelist.Rmd`  should be run. This file follows has three main phases:

- Retrieval:
	+ This step copies all required input files from dropbox into the `inputs` folder.
- Process:
	+ This phase runs takes the input files and processes them using the scripts `ECE_spatialdata_tidying.R`, `ECE_upsampling.R`, and `school_spatialdata_tidying.R`
	+ It will also clean the tables to remove white space from file names, and put tables into a tidy format.
- Publish
	+ This step takes the final output folder and copies back to the dropbox.
	
The notebook should run for all users. The only necessary change will be to ensure that the variable "dropbox_directory" is correctly specified to the users local machine.

### analysis

The `analysis` folder contains any code used for subsequent analysis of data, such as summary statistics or maps. 

### inputs

The `inputs` folder contains any input files required for `processing` and additional files reuqired for `analysis`.

The main required inputs for `processing` are:

- `schooldirectory.csv`. This is obtained directly from education counts website: https://www.educationcounts.govt.nz/data-services/directories
Data obtained from educationcounts 26/03/20

- `Directory-ECE-Dec2019.csv`. Obtained directly from education counts website: https://www.educationcounts.govt.nz/directories/api-new-zealand-institutions

### outputs

The `outputs` folder will be the target folder for any results saved by `processing` or `analysis` files.

## Authors and acknowledgment

Authors (order alphabetical): James Gilmour, Emily Harvey, Dion O'Neale, Steven Turnbull

This work was funded and supported by Te P\={u}naha Matatini and MBIE.

## License

Shield: [![CC BY-NC-SA 4.0][cc-by-nc-sa-shield]][cc-by-nc-sa]

This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0
International License][cc-by-nc-sa].

[![CC BY-NC-SA 4.0][cc-by-nc-sa-image]][cc-by-nc-sa]

[cc-by-nc-sa]: http://creativecommons.org/licenses/by-nc-sa/4.0/
[cc-by-nc-sa-image]: https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png
[cc-by-nc-sa-shield]: https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg
